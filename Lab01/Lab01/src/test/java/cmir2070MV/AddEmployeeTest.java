package cmir2070MV;

import cmir2070MV.exception.EmployeeException;
import cmir2070MV.repository.fileRepository.EmployeeFileRepository;
import org.junit.Before;
import org.junit.Test;
import cmir2070MV.controller.EmployeeController;
import cmir2070MV.model.DidacticFunction;
import cmir2070MV.repository.interfaces.EmployeeRepositoryInterface;
import cmir2070MV.validator.EmployeeValidator;

import static org.junit.Assert.assertEquals;

public class AddEmployeeTest {

	private EmployeeRepositoryInterface employeeRepository;
	private EmployeeController controller;
	private EmployeeValidator employeeValidator;
	
	@Before
	public void setUp() {
		employeeRepository = new EmployeeFileRepository("employeesTest.txt");
		employeeValidator  = new EmployeeValidator();
		controller         = new EmployeeController(employeeRepository, employeeValidator);
	}
	
	@Test
	public void addEmployeeECPValid() throws EmployeeException {
		int oldSize = controller.getEmployeesList().size();
		controller.addEmployee("Nume", "1123456789123", DidacticFunction.ASISTENT, "1020");
		int newSize = controller.getEmployeesList().size();
		assertEquals(oldSize+1, newSize);
	}

	@Test(expected = EmployeeException.class)
	public void addEmployeeECPInvalid() throws EmployeeException {
		controller.addEmployee("Nume02", "1123456789123", DidacticFunction.ASISTENT, "1020");

	}

	@Test
	public void addEmployeeBVAValid() throws EmployeeException {
		int oldSize = controller.getEmployeesList().size();
		controller.addEmployee("Marius", "1123456789123", DidacticFunction.ASISTENT, "1020");
		int newSize = controller.getEmployeesList().size();
		assertEquals(oldSize+1, newSize);
	}

	@Test(expected = EmployeeException.class)
	public void addEmployeeBVAInvalid() throws EmployeeException {
		controller.addEmployee("Nume", "112345678912", DidacticFunction.ASISTENT, "1020");

	}



}
