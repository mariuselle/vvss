package cmir2070MV.repository.fileRepository;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

import cmir2070MV.exception.EmployeeException;

import cmir2070MV.model.DidacticFunction;
import cmir2070MV.model.Employee;

import cmir2070MV.repository.interfaces.EmployeeRepositoryInterface;
import cmir2070MV.validator.EmployeeValidator;

public class EmployeeFileRepository implements EmployeeRepositoryInterface {
	
	private String employeeDBFile;
	private ArrayList<Employee> all;

	public EmployeeFileRepository(String employeeDBFile) {
		this.employeeDBFile = employeeDBFile;
		this.all = new ArrayList<>();
		loadAll();
	}



	@Override
	public void addEmployee(Employee employee) {
		this.all.add(employee);
		writeAll();
	}

	@Override
	public void deleteEmployee(Employee employee) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void modifyEmployee(Employee oldEmployee, Employee newEmployee) throws EmployeeException {
		BufferedWriter bw;
		try {
			bw = new BufferedWriter(new FileWriter(employeeDBFile));
			for (Employee employee: this.all) {
				if (employee.getCnp().equals(oldEmployee.getCnp())) {
					bw.write(newEmployee.toString());
				} else {
					bw.write(employee.toString());
				}

				bw.newLine();
        	}
			bw.close();

            this.all.remove(oldEmployee);
            this.all.add(newEmployee);

        } catch (IOException e) {
            throw new EmployeeException("File not exist!");
		}

	}

	@Override
	public List<Employee> getEmployeeList() {
		return this.all;
	}

	private void writeAll() {
		BufferedWriter bw;
		try {
			bw = new BufferedWriter(new FileWriter(employeeDBFile));
			for (Employee employee: this.all) {
				bw.write(employee.toString());
				bw.newLine();
			}
			bw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void loadAll() {

		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(employeeDBFile));
			String line;
			int counter = 0;
			while ((line = br.readLine()) != null) {
				Employee employee = new Employee();
				try {
					employee = getEmployeeFromString(line, counter);
					this.all.add(employee);
				} catch(EmployeeException ex) {
					System.err.println("Error while reading: " + ex.toString());
				}
				counter ++;

			}
		} catch (IOException e) {
			System.err.println("Error while reading: " + e);
		} finally {
			if (br != null)
				try {
					br.close();
				} catch (IOException e) {
					System.err.println("Error while closing the file: " + e);
				}
		}
		
	}


	@Override
	public List<Employee> getEmployeeByCriteria(String criteria) {

		return new ArrayList<>();
	}

	/**
	 * Returns the Employee after parsing the given line
	 *
	 * @param _employee
	 *            the employee given as String from the input file
	 * @param line
	 *            the current line in the file
	 *
	 * @return if the given line is valid returns the corresponding Employee
	 * @throws EmployeeException
	 */
	private Employee getEmployeeFromString(String _employee, int line) throws EmployeeException {
		Employee employee = new Employee();

		String[] attributes = _employee.split("[;]");

		if( attributes.length != 4 ) {
			throw new EmployeeException("Invalid line at: " + line);
		} else {
			EmployeeValidator validator = new EmployeeValidator();
			employee.setLastName(attributes[0]);
			employee.setCnp(attributes[1]);

			if(attributes[2].equals("ASISTENT"))
				employee.setFunction(DidacticFunction.ASISTENT);
			if(attributes[2].equals("LECTURER"))
				employee.setFunction(DidacticFunction.LECTURER);
			if(attributes[2].equals("TEACHER"))
				employee.setFunction(DidacticFunction.TEACHER);
			if (attributes[2].equals("CONFERENTIAR"))
				employee.setFunction(DidacticFunction.CONFERENTIAR);
			employee.setSalary(attributes[3]);

			if( !validator.isValid(employee) ) {
				throw new EmployeeException("Invalid line at: " + line);
			}
		}

		return employee;
	}

}
